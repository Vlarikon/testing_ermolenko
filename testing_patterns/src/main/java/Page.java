import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Page {
    private WebDriver driver;
    private WebDriverWait wait;

    public Page(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    public void open() {
        driver.get("https://playground.learnqa.ru/puzzle/triangle");
    }

    @FindBy(id = "show_answ")
    private WebElement showAnswers;

    @FindBy(linkText = "Ссылка на ответы")
    private WebElement answersLink;

    @FindBy(id = "hide_answ")
    private WebElement hideAnswers;

    public boolean isButtonShowAnswersExists() {
        return showAnswers.isDisplayed();
    }

    public boolean isAnswersLinkExists() {
        showAnswers.click();
        wait.until(ExpectedConditions.elementToBeClickable(answersLink));
        return answersLink.isDisplayed();
    }

    public boolean isButtonHideAnswersExists() {
        return hideAnswers.isDisplayed();
    }
}