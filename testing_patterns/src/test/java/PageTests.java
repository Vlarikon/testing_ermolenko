import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.*;


public class PageTests {
    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(968, 576));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void pageTests() {
        Page page = PageFactory.initElements(driver, Page.class);
        page.open();
        assertTrue(page.isButtonShowAnswersExists());
        assertTrue(page.isAnswersLinkExists());
        assertFalse(page.isButtonShowAnswersExists());
        assertTrue(page.isButtonHideAnswersExists());
    }
}