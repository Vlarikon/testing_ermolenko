public class CustomMatchersAssertion {

    public static ArrayMatcher assertThat(int ... actual) {
        return new ArrayMatcher(actual);
    }

    public static JsonMatcher assertThat(String actual) {
        return new JsonMatcher(actual);
    }
}