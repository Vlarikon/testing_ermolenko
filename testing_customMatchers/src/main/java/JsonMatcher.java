import org.assertj.core.api.AbstractAssert;
import org.json.JSONObject;

public class JsonMatcher extends AbstractAssert<JsonMatcher, String> {

    public JsonMatcher(String actual) {
        super(actual, JsonMatcher.class);
    }

    public JsonMatcher assertThat(String actual) {
        return new JsonMatcher(actual);
    }

    public JsonMatcher hasField(String field) {
        isNotNull();
        if(field == null || field.isEmpty()) {
            throw new NullPointerException("Parameter must be not null and not empty");
        }
        JSONObject obj = new JSONObject(this.actual);
        if(!obj.has(field)) {
            failWithMessage("Doesn't have field <%s>", field);
        }
        return this;
    }

    public JsonMatcher hasNoField(String field) {
        isNotNull();
        if(field == null || field.isEmpty()) {
            throw new NullPointerException("Parameter must be not null and not empty");
        }
        JSONObject obj = new JSONObject(this.actual);
        if(obj.has(field)) {
            failWithMessage("There is field <%s>", field);
        }
        return this;
    }

    public JsonMatcher hasValueInField(String field) {
        isNotNull();
        hasField(field);
        if(field == null || field.isEmpty()) {
            throw new NullPointerException("Parameter must be not null and not empty");
        }
        JSONObject obj = new JSONObject(this.actual);
        if(obj.isNull(field) || obj.get(field).equals("")) {
            failWithMessage("No value in field <%s>", field);
        }
        return this;
    }

    public JsonMatcher hasNoValueInField(String field) {
        isNotNull();
        hasField(field);
        if(field == null || field.isEmpty()) {
            throw new NullPointerException("Parameter must be not null and not empty");
        }
        JSONObject obj = new JSONObject(this.actual);
        if(!obj.isNull(field) && !obj.get(field).equals("")) {
            failWithMessage("Field <%s> has value", field);
        }
        return this;
    }
}