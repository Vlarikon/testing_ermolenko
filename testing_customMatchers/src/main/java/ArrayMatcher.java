import org.assertj.core.api.AbstractAssert;

import java.util.HashSet;
import java.util.Set;

public class ArrayMatcher extends AbstractAssert<ArrayMatcher, int[]> {

    public ArrayMatcher(int ... actual) {
        super(actual, ArrayMatcher.class);
    }

    public ArrayMatcher containsSimilarNumbers(int... arr) {
        boolean flag = false;
        Set<Integer> set = new HashSet<>();
        for(int i : this.actual) {
            set.add(i);
        }
        for(int i : arr) {
            flag = set.contains(i);
            if(flag) {
                break;
            }
        }
        if(!flag) {
            failWithMessage("Arrays have no matches");
        }
        return this;
    }

    public ArrayMatcher NotContainsSimilarNumbers(int... arr) {
        boolean flag = false;
        Set<Integer> set = new HashSet<>();
        for(int i : this.actual) {
            set.add(i);
        }
        for(int i : arr) {
            flag = set.contains(i);
            if(flag) {
                break;
            }
        }
        if(flag) {
            failWithMessage("Arrays have matches");
        }
        return this;
    }
}