import org.junit.jupiter.api.Test;

public class CustomMatchersTest {

    @Test
    public void arrayTests() {
        int[] arr1 = new int[]{1, 2, 3, 4, 5, 6};
        int[] arr2 = new int[]{3, 2, 1};
        int[] arr3 = new int[]{4, 6, 5};
        int[] arr4 = new int[]{7, 8, 9};
        CustomMatchersAssertion.assertThat(arr1).containsSimilarNumbers(arr2);
        CustomMatchersAssertion.assertThat(arr1).containsSimilarNumbers(arr3);
        CustomMatchersAssertion.assertThat(arr1).NotContainsSimilarNumbers(arr4);
    }

    @Test
    public void jsonTests() {
        String json1 =  "{\"status\": \"ok\", \"success\": \"true\"}";
        String json2 =  "{\"notstatus\": \"ok\", \"success\": \"\"}";
        CustomMatchersAssertion.assertThat(json1).hasField("status").hasValueInField("success");
        CustomMatchersAssertion.assertThat(json2).hasNoField("status").hasNoValueInField("success");
    }
}