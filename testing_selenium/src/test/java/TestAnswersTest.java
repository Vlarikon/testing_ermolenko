import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import java.util.*;

public class TestAnswersTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://playground.learnqa.ru/puzzle/triangle");
        driver.manage().window().setSize(new Dimension(968, 576));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testAnswers() {
        {
            List<WebElement> elements = driver.findElements(By.id("show_answ"));
            assertTrue(elements.size() > 0);
            assertTrue(elements.get(0).isDisplayed());
        }
        driver.findElement(By.id("show_answ")).click();
        {
            List<WebElement> elements = driver.findElements(By.linkText("Ссылка на ответы"));
            assertTrue(elements.size() > 0);
            assertTrue(elements.get(0).isDisplayed());
        }
        {
            List<WebElement> elements = driver.findElements(By.id("show_answ"));
            assertFalse(elements.get(0).isDisplayed());
        }
        {
            List<WebElement> elements = driver.findElements(By.id("hide_answ"));
            assertTrue(elements.size() > 0);
            assertTrue(elements.get(0).isDisplayed());
        }
        driver.findElement(By.id("hide_answ")).click();
        {
            List<WebElement> elements = driver.findElements(By.id("show_answ"));
            assertTrue(elements.get(0).isDisplayed());
        }
        {
            List<WebElement> elements = driver.findElements(By.linkText("Ссылка на ответы"));
            assertEquals(0, elements.size());
        }
        {
            List<WebElement> elements = driver.findElements(By.id("hide_answ"));
            assertFalse(elements.get(0).isDisplayed());
        }
    }
}