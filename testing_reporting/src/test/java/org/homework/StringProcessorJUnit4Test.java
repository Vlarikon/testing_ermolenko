package org.homework;

import org.junit.Test;
import org.testng.annotations.Listeners;

import static org.homework.StringProcessor.copyStrocs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@Listeners({ CustomisedListener.class })
public class StringProcessorJUnit4Test {

   @Test
    public void testCopy() throws IllegalArgumentException {
        assertEquals(copyStrocs("N", 3), "NNN");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCopyExc() throws IllegalArgumentException {
        copyStrocs("qwe", -6);
        fail();
    }

    @Test
    public void testFind() {
        assertEquals(StringProcessor.quantity("aaa", "aa"), 2);
    }

}