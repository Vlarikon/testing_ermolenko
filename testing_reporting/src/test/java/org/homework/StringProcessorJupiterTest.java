package org.homework;

import org.junit.jupiter.api.Test;
import org.testng.annotations.Listeners;

import static org.homework.StringProcessor.copyStrocs;
import static org.junit.jupiter.api.Assertions.*;

@Listeners({ CustomisedListener.class })
public class StringProcessorJupiterTest {

   @Test
    public void testCopy() throws IllegalArgumentException {
        assertAll(
                ()->assertEquals("NNN", copyStrocs("N", 3)),
                ()->assertEquals("", copyStrocs("", 1))
        );
    }

    @Test
    public void testCopyExc() throws IllegalArgumentException {
        assertThrows(IllegalArgumentException.class, () ->copyStrocs("qwe", -6));
    }

    @Test
    public void testFind() {
        assertEquals(2, StringProcessor.quantity("aaa", "aa"));
    }

}