package org.homework;

import io.qameta.allure.*;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;
import static org.homework.StringProcessor.copyStrocs;
import static org.homework.StringProcessor.delete;

@Listeners({ CustomisedListener.class })
@Epic("TestNG framework is used for tests")
@Feature("String Processor")
public class StringProcessorTestNGTest extends BaseTest{
    @AfterClass
    public void tearDown() {
        getDriver().quit();
    }

    @DataProvider
    public static Object[][] data() {
        return new Object[][]{
                {"N", 3, "NNN"},
                {"qwe", 3, "qweqweqwe"},
                {"", 3, ""},
                {"", 0, " "}
        };
    }

    @DataProvider
    public static Object[][] countData() {
        return new Object[][]{
                {"a", "a", 1},
                {"aa", "a", 2},
                {"aaa", "aa", 2},
                {"", "aaa", 0} ,
//                {"", "", 0}
//                {"asdf", "", -1},
                {"aaaa", "vvv", 0},
                {"aeqra facwda", "a", 4}
        };
    }

    @DataProvider
    public static Object[][] onetwothreeData() {
        return new Object[][]{
                {"", ""},
                {"567890", "567890"},
                {" 1 ", " один "},
                {" 2", " два"},
                {"1 2 3", "один два три"}
        };
    }

    @DataProvider
    public static Object[][] changeWords() {
        return new Object[][]{
                {"", ""},
                {" qq ", " qq "},
                {"   123 erty 11 ", "   11 erty 123 "}
        };
    }

    @DataProvider
    public static Object[][] changeAge() {
        return new Object[][]{};
    }

    @Description(value="test copying of string")
    @Story("Copy String")
    @Test(  description="copying of string",
            dataProvider = "data"
    )
    public void testCopy(String str, int N, String expected) throws IllegalArgumentException {
        assertEquals(copyStrocs(str, N), expected);
    }

    @Story("Copy String Exception")
    @Description(value="Test copying function throws an IllegalArgumentException")
    @Test(
            expectedExceptions = {IllegalArgumentException.class},
            description = "string copying exception"
    )
    public void testCopyExc() throws IllegalArgumentException {
        copyStrocs("qwe", -6);
        fail();
    }

    @Story("Does a string contain a smaller one? (amount)")
    @Description(value="Count of how many times small string is contained in the big one")
    @Test(dataProvider = "countData", description = "string contains the smaller one")
    public void testFind(String big, String small, int expected) {
        assertEquals(StringProcessor.quantity(big, small), expected);
    }

    @Story("Replace digits 1,2,3 to the words")
    @Test(dataProvider = "onetwothreeData", description = "replace digit with word")
    public void testChange(String source, String expected) {
        assertEquals(StringProcessor.strocs(source), expected);
    }

    @Flaky
    @Test(description = "delete even chars from the string")
    public void testDeleteEvenCharacter() {
        StringBuilder  stringBuilder = new StringBuilder("123456789abc");
        StringBuilder temp = delete(stringBuilder);
        assertEquals(temp.toString(), "13579b");
        temp = delete(stringBuilder);
        assertEquals(temp.toString(), "159");
        temp = delete(stringBuilder);
        assertEquals(temp.toString(), "19");
        temp = delete(stringBuilder);
        assertEquals(temp.toString(), "1");
    }

    @Test(enabled = false, dataProvider = "changeWords")
    public void testChange3(String source, String expected) {
       assertEquals( replaceStr(new StringBuilder(source)).toString(), expected);
    }


    @Test(enabled = false, dataProvider = "changeAge")
    public void testChange4(String source, String expected) {
        assertEquals(hexadecimalNotation(source), expected);
    }

/****** fake functions *******************************/
    private StringBuilder replaceStr(StringBuilder stringBuilder) {
        return new StringBuilder("123");
    }

    private String hexadecimalNotation(String source) {
        return "123";
    }

}